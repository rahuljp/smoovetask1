package com.example.task1.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.task1.R
import com.example.task1.databinding.ItemListInterestBinding
import com.example.task1.models.response.Interest
import com.example.task1.utils.showToast
import com.google.android.material.card.MaterialCardView
import java.awt.font.TextAttribute
import java.time.format.TextStyle

class InterestAdapter(
    private var listInterest : List<Interest>,
) : RecyclerView.Adapter<InterestAdapter.viewholder>() {




    var context : Context?=null

    private  var total_selected_count= arrayListOf<Interest>()

    class viewholder(val binding: ItemListInterestBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewholder {
        context=parent.context
        return viewholder(ItemListInterestBinding.inflate(LayoutInflater.from(context),parent,false))
    }

    override fun onBindViewHolder(holder: viewholder, position: Int) {
        var interest=listInterest[holder.absoluteAdapterPosition]
        var cardview = holder.binding.cardview2

        Glide.with(context!!)
            .load(interest.dark_icon_url)
            .into(holder.binding.interestImg)

        holder.binding.interestName.text=interest.name
        holder.binding.interestName.setTypeface(null,Typeface.NORMAL)


        manageSelection(interest,holder,cardview)


        setCardViewSelectListener(cardview,interest,holder)



    }

    private fun manageSelection(interest: Interest,holder: viewholder,cardview: MaterialCardView) {
        if (interest.selected){

            if (!total_selected_count.contains(interest)) total_selected_count.add(interest)

            Glide.with(context!!)
                .load(interest.color_icon_url)
                .into(holder.binding.interestImg)

            holder.binding.interestName.setTypeface(null,Typeface.BOLD)
            cardview.strokeWidth=4
        }
        else{
            Glide.with(context!!)
                .load(interest.dark_icon_url)
                .into(holder.binding.interestImg)

            holder.binding.interestName.setTypeface(null,Typeface.NORMAL)
            cardview.strokeWidth=0
        }
    }

    private fun setCardViewSelectListener(cardview : MaterialCardView, interest : Interest, holder: viewholder) {
        cardview.setOnClickListener{
            if (interest.selected){
                if (total_selected_count.contains(interest))  total_selected_count.remove(interest)
                interest.selected=false
                Glide.with(context!!)
                    .load(interest.dark_icon_url)
                    .into(holder.binding.interestImg)

                holder.binding.interestName.setTypeface(null,Typeface.NORMAL)
                cardview.strokeWidth=0


            }
            else{
                if (total_selected_count.size==6) {
                    context?.showToast("Only Six Interest allowed for Selection")
                    return@setOnClickListener
                }

                if (!total_selected_count.contains(interest)) total_selected_count.add(interest)
                interest.selected=true
                Glide.with(context!!)
                    .load(interest.color_icon_url)
                    .into(holder.binding.interestImg)

                holder.binding.interestName.setTypeface(null,Typeface.BOLD)
                cardview.strokeWidth=4

            }
        }
    }

    override fun getItemCount(): Int {
        return listInterest.size
    }


}