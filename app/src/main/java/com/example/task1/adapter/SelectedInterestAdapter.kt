package com.example.task1.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.task1.R
import com.example.task1.databinding.ItemListInterestBinding
import com.example.task1.models.response.Interest
import java.awt.font.TextAttribute
import java.time.format.TextStyle

class SelectedInterestAdapter(
    private var listInterest : List<Interest>,
) : RecyclerView.Adapter<SelectedInterestAdapter.viewholder>() {




    var context : Context?=null
    class viewholder(val binding: ItemListInterestBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewholder {
        context=parent.context
        return viewholder(ItemListInterestBinding.inflate(LayoutInflater.from(context),parent,false))
    }

    override fun onBindViewHolder(holder: viewholder, position: Int) {
        var interest=listInterest[holder.absoluteAdapterPosition]
        var cardview = holder.binding.cardview2


        Glide.with(context!!)
            .load(interest.color_icon_url)
            .into(holder.binding.interestImg)

        holder.binding.interestName.setTypeface(null,Typeface.BOLD)
        cardview.strokeWidth=4

        holder.binding.interestName.text=interest.name

    }

    override fun getItemCount(): Int {
        return listInterest.size
    }
}