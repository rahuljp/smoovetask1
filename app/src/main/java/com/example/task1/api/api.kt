package com.example.task1.api

import com.example.task1.models.response.ListInterestModel
import retrofit2.Response
import retrofit2.http.POST

interface api {

    companion object{
        const val smoove_api="smoove-api/api/v1/"

        const val list_interest = "${smoove_api}list-interest"
    }

    @POST(list_interest)
    suspend fun listInterest() : Response<ListInterestModel>

}