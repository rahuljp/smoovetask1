package com.example.task1.data.Database

import androidx.room.*
import com.example.task1.data.Database.Entities.Interest

@Dao
interface AppDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInterest(listInterest : List<Interest>)

    @Query("SELECT * FROM INTEREST")
    suspend fun getListOfInterest() : List<Interest>

    @Update
    suspend fun updateInterest(list :List<Interest>)


}