package com.example.task1.data.Database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.task1.data.Database.Entities.Interest


@Database(entities = [Interest :: class] , version = 1 ,exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getDao() : AppDao
}