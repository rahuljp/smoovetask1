package com.example.task1.data.Database.Entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.task1.models.response.Interest

@Entity
data class Interest(
    val color_icon: String,
    val color_icon_url: String,
    val created_at: String,
    val created_by: String?,
    val dark_icon: String,
    val dark_icon_url: String,
    val deleted_at: String?,
    @PrimaryKey(autoGenerate = false) val id: Int,
    val name: String,
    val type: String,
    val updated_at: String,
    val updated_by: String?,
    val selected : Boolean = false
)
{
    fun toInterest() : Interest{
        return Interest(
            color_icon,color_icon_url,created_at,created_by,dark_icon,dark_icon_url,deleted_at,id,name,type,updated_at,updated_by,selected
        )
    }
}