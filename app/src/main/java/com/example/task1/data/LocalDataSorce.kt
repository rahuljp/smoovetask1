package com.example.task1.data

import com.example.task1.data.Database.AppDao
import com.example.task1.data.Database.Entities.Interest
import javax.inject.Inject

class LocalDataSorce @Inject constructor(
    private val appDao: AppDao
) {

    suspend fun insertInterest(listInterest : List<Interest>){
        return appDao.insertInterest(listInterest)
    }

    suspend fun getListOfInterest(): List<Interest> {
        return appDao.getListOfInterest()
    }

    suspend fun updateInterest(list : List<Interest>) {
        return appDao.updateInterest(list)
    }

}