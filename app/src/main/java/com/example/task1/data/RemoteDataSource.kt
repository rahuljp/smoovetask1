package com.example.task1.data

import com.example.task1.api.api
import com.example.task1.models.response.ListInterestModel
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val api: api
) {
    suspend fun getListInterest(): Response<ListInterestModel> {
        return api.listInterest()
    }
}