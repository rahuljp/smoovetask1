package com.example.task1.data

import javax.inject.Inject

class Repository @Inject constructor(
    localDataSorce: LocalDataSorce,
    remoteDataSource: RemoteDataSource
) {
    val local=localDataSorce
    val remote=remoteDataSource

}