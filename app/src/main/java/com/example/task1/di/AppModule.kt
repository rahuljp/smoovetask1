package com.example.task1.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.task1.data.Database.AppDao
import com.example.task1.data.Database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideAppDatabase(
        @ApplicationContext context: Context
    ): AppDatabase {
        return Room.databaseBuilder(context,AppDatabase::class.java,"AppDtabase.db")
            .build()
    }

    @Singleton
    @Provides
    fun provideAppDao(
        appDatabase: AppDatabase
    ):AppDao{
        return appDatabase.getDao()
    }
}