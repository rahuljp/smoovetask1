package com.example.task1.di

import com.example.task1.api.api
import com.example.task1.utils.URLs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    class SupportInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            var request = chain.request()
            request = request.newBuilder()
                .addHeader(
                    "Authorization","Bearer ${URLs.token}"
                )
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build()

            return chain.proceed(request)
        }
    }

    @Singleton
    @Provides
    fun provideRetrofitInstance(
        gsonConverterFactory: GsonConverterFactory,
        httpClient: OkHttpClient
    ) : Retrofit{
        return Retrofit.Builder()
            .baseUrl(URLs.base_url)
            .addConverterFactory(gsonConverterFactory)
            .client(httpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideApiInstance(
        retrofit: Retrofit
    ) : api {
        return retrofit.create(api ::class.java)
    }

    @Singleton
    @Provides
    fun provideGsonConverterFactory() : GsonConverterFactory{
        return GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun providerHttpClient(
    ): OkHttpClient {


        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor(SupportInterceptor())
            .build()
    }



}