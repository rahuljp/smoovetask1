package com.example.task1.models.response

data class Data(
    val interests: List<Interest>
)