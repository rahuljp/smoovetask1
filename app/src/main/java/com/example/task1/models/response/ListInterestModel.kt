package com.example.task1.models.response

data class ListInterestModel(
    val `data`: Data,
    val message: String,
    val status: String
)
