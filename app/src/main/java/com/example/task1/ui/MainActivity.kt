package com.example.task1.ui

import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.task1.R
import com.example.task1.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding : ActivityMainBinding
    private  var navController: NavController?=null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


//        binding.toolbar.title="My Profile"
//        binding.toolbar.setTitleTextColor(getColor(R.color.white))
//        setSupportActionBar(binding.toolbar)



        setClickListeners()

         navController = (supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment ).navController

    }

    private fun setClickListeners() {
        binding.editInterest.setOnClickListener{
            navController?.navigate(R.id.action_myProfileFragment_to_listInterestBottomSheet2)
        }
    }



}