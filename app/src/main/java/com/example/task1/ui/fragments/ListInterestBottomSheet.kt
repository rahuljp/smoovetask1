package com.example.task1.ui.fragments

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.task1.R
import com.example.task1.adapter.InterestAdapter
import com.example.task1.databinding.FragmentListInterestBottomSheetBinding
import com.example.task1.models.response.Interest
import com.example.task1.viewmodels.ListInterestBottomSheetViewModel
import com.example.task1.viewmodels.MyProfileFragmentViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListInterestBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding : FragmentListInterestBottomSheetBinding
    private lateinit var viewmodel : ListInterestBottomSheetViewModel
    private var interest_data : List<Interest> = listOf()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListInterestBottomSheetBinding.inflate(inflater,container,false)
        viewmodel=ViewModelProviders.of(this).get(ListInterestBottomSheetViewModel::class.java)

        binding.listinterestAllRv.layoutManager=GridLayoutManager(context,3,LinearLayoutManager.VERTICAL,false)

        viewmodel.getInterest()

        setObservers()
        setClickListener()

        return binding.root
    }

    private fun setClickListener() {
        binding.button.setOnClickListener{
            viewmodel.updateDatabase(interest_data)
            (parentFragment as NavHostFragment).navController.navigate(R.id.action_listInterestBottomSheet_to_myProfileFragment)
        }
    }

    private fun setObservers() {
        viewmodel.getListInterestData().observe(this,{
            interest_data=it
            binding.listinterestAllRv.adapter=InterestAdapter(interest_data)
        })

    }

}