package com.example.task1.ui.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.task1.R
import com.example.task1.adapter.InterestAdapter
import com.example.task1.adapter.SelectedInterestAdapter
import com.example.task1.databinding.FragmentMyProfileBinding
import com.example.task1.utils.showToast
import com.example.task1.viewmodels.MyProfileFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyProfileFragment : Fragment() {


    private lateinit var binding : FragmentMyProfileBinding
    private lateinit var  viewmodel : MyProfileFragmentViewModel



    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentMyProfileBinding.inflate(inflater,container,false)

        viewmodel=ViewModelProviders.of(this).get(MyProfileFragmentViewModel::class.java)




        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewmodel.getInterest()
        setObservers()


        binding.interestRv.layoutManager=
            LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)


        binding.swiperefresh.setOnRefreshListener {
            viewmodel.getInterest()
        }

    }


    private fun setObservers() {
        viewmodel.getListInterestData().observe(this,{
                response ->
            //context?.showToast(response.size.toString())
            if (binding.swiperefresh.isRefreshing) binding.swiperefresh.isRefreshing=false
            binding.interestRv.adapter= SelectedInterestAdapter(response)
        })
    }







}