package com.example.task1.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.example.task1.R
import com.example.task1.models.response.ListInterestModel
import com.example.task1.utils.showToast
import retrofit2.Response

open class BaseViewModel(
    application: Application) : AndroidViewModel(application) {

    private var context =application


    @RequiresApi(Build.VERSION_CODES.M)
    fun isConnected(): Boolean {
        val connectivityManager =
            getApplication<Application>().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (connectivityManager.activeNetwork == null) {
            context.showToast(context.getString(R.string.no_internet))
            false
        } else {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities =
                connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    true
                }
                else -> {
                    context.showToast(context.getString(R.string.no_internet))
                    false
                }
            }
        }
    }
    fun<T> handleResponse(response: Response<T>) : T? {
        if (response.isSuccessful){
            var model_data : T?= response.body()
            return model_data
        }
        else{
            context.showToast(response.message())
        }

        return null

    }
}