package com.example.task1.viewmodels

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.task1.api.api
import com.example.task1.data.Repository
import com.example.task1.models.response.Interest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListInterestBottomSheetViewModel @Inject constructor(
    private val repository: Repository,
    private val api: api,
    application: Application)
    : BaseViewModel(application) {

    init {

    }

    private val list_interest : MutableLiveData<List<Interest>> = MutableLiveData()

    fun getListInterestData() : LiveData<List<Interest>> {
        return list_interest
    }
    @RequiresApi(Build.VERSION_CODES.M)
    fun getInterest() = viewModelScope.launch {
        Log.d("in bottom sheet","bottom sheet")
        getListInterest()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private suspend fun getListInterest() {
        var local_data=repository.local.getListOfInterest()
        if (!local_data.isNullOrEmpty()){
            list_interest.postValue(local_data.map { it.toInterest() })
            return
        }
        if (isConnected()) {
            var response = api.listInterest()
            var model_data = handleResponse(response)
            if (model_data != null) {
                var listdata=model_data.data.interests
                repository.local.insertInterest(listdata.map { it.toInterest() })
                list_interest.postValue(listdata)
            }
        }

    }


    fun updateDatabase(list : List<Interest>) = viewModelScope.launch {
        repository.local.updateInterest(list.map { it.toInterest() })
    }

}