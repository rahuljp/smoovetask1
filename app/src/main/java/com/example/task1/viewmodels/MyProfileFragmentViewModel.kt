package com.example.task1.viewmodels

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.task1.api.api
import com.example.task1.data.Repository
import com.example.task1.models.response.Interest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyProfileFragmentViewModel @Inject constructor(
    private val repository: Repository,
    private val api: api,
    application: Application) : BaseViewModel(application) {

    init {

    }

    private val list_interest : MutableLiveData<List<Interest>> = MutableLiveData()


    fun getListInterestData() : LiveData<List<Interest>> {
        return list_interest
    }
    @RequiresApi(Build.VERSION_CODES.M)
    fun getInterest() = viewModelScope.launch {
        getListInterest()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun getListInterest() {
        var local_data=repository.local.getListOfInterest()

        var selected_data:ArrayList<Interest> = arrayListOf()
        if (!local_data.isNullOrEmpty()){
            local_data.forEach{
                if (it.selected){
                    selected_data.add(it.toInterest())
                }
            }

            list_interest.postValue(selected_data)
            return
        }
    }
}